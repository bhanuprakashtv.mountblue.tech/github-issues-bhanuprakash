import React, {Component} from 'react';
import issuesdata from './issues.json';
import arraySort from 'array-sort';
import './App.css';
import IssuesComponent from './components/Issues/Issues';
import Header from './components/header-components/Header';
import Fuse from 'fuse.js';
class App extends Component {
  state = {
    issues: issuesdata,
  };

  filterInput = () => {
    let author = [
      ...new Set (
        issuesdata.reduce ((obj, val) => {
          obj.push (val.user.login);
          return obj;
        }, [])
      ),
    ];

    let labelList = [
      ...new Set (
        issuesdata.reduce ((obj, val) => {
          val.labels.filter (value => obj.push (value.name));
          return obj;
        }, [])
      ),
    ];
    let filterInputData = {
      author: author,
      state: ['open', 'close'],
      labels: labelList,
    };
    return filterInputData;
  };

  //  return noOfMatches;
  // }

  sortResult = val => {
    //console.log (this.state.issues);
    let result;
    switch (val) {
      case 'newest': {
        result = arraySort (this.state.issues, 'created_at', {reverse: true});
        break;
      }
      case 'oldest': {
        result = arraySort (this.state.issues, 'created_at');
        break;
      }
      case 'recently updated': {
        result = arraySort (this.state.issues, 'updated_at', {reverse: true});
        break;
      }
      case 'least recently updated': {
        result = arraySort (this.state.issues, 'updated_at');
        break;
      }
    }
    this.setState ({
      issues: result,
    });
  };

  filterResult = object => {
    let key = 'state';
    if (object.type === 'Author') key = 'user.login';
    else if (object.type === 'Labels') key = 'labels.name';

    var options = {
      threshold: 0,
      keys: [key],
    };
    var fuse = new Fuse (issuesdata, options);
    let result = fuse.search (object.value);
    this.setState ({
      issues: result,
    });
  };

  searchResult = (val) => {
     if (val.length === 0){
      this.setState({issues:issuesdata})
      return;
    }
    var options = {
      keys: ['title'],
    };
    var fuse = new Fuse (issuesdata, options);
    let result = fuse.search (val);
    this.setState ({
      issues: result,
    });
  };

  render () {
    let filterMenuData = this.filterInput ();
    //console.log(filterInputData)          sortInput={this.sortResult}
    return (
      <section className="App">
        <Header
          searchInputValue={this.searchResult}
          sortInputValue={this.sortResult}
          filterInputValue={this.filterResult}
          filterMenuData={filterMenuData}
        />
        <IssuesComponent issues={this.state.issues} />
      </section>
    );
  }
}

export default App;
