import React, {Component} from 'react';
import moment from "moment";

class IssueSubTitle extends Component {
  state = {};
  render () {
    return (
       <span className="issue-details">
          #{this.props.issue.number} opened{' '}
          {moment (this.props.issue.created_at.split ('T')[0], 'YYYY-MM-DD').fromNow ()}
          {' '}
          by
          {' '}
          {this.props.issue.user.login}{' '}
        </span>
    );
  }
}

export default IssueSubTitle;
