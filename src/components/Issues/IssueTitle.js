import React, {Component} from 'react';
import Chip from '@material-ui/core/Chip';

class IssueTitle extends Component {
  state = {};
  render () {
    return (
      <React.Fragment>
        {this.props.issue.title}
        <span>
          {this.props.issue.labels.map (label => {
            return (
              <Chip
                style={{backgroundColor: '#' + label.color,margin:"0em 1em"}}
                label={label.name}
                key={label.id}
              />
            );
          })}
        </span>
      </React.Fragment>
    );
  }
}

export default IssueTitle;
