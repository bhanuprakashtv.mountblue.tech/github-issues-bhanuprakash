import React, {Component} from 'react';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';

import './Issues.css';
import IssueTitle from './IssueTitle';
import IssueSubTitle from './IssueSubTitle';

class Issues extends Component {
  state = {};
  render () {
    //console.log (this);
    return ( 
      <List component="nav" >
        {this.props.issues.map (issue => {
          return (
            <ListItem button key={issue.id} className="issues-list">
              <ListItemText
                primary={<IssueTitle issue={issue} />}
                secondary={<IssueSubTitle issue={issue} />}
              />
            </ListItem>
          );
        })}
      </List>
    );
  }
}

export default Issues;
