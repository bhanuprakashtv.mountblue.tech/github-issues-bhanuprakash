import React, {Component} from 'react';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Collapse from '@material-ui/core/Collapse';
import ExpandLess from '@material-ui/icons/ExpandLess';
import ExpandMore from '@material-ui/icons/ExpandMore';
class FilterItem extends Component {
  state = {
    open: false,
  };
  handleClick = () => {
    this.setState (state => ({open: !state.open}));
  };

  filter = e => {
    this.setState (state => ({open: !state.open}));
    this.props.filterInputValue ({
      type:this.props.title,
      value:e.target.textContent});
  };
  render () {
    //console.log (this);
    return (
      <List component="nav">
        <ListItem button onClick={this.handleClick}>
          <ListItemText inset primary={this.props.title} />
          {this.state.open ? <ExpandLess /> : <ExpandMore />}
        </ListItem>

        <Collapse in={this.state.open} timeout="auto" unmountOnExit>
          {this.props.filterMenuData.map (value => {
            return (
              <List component="div" disablePadding key={value}>
                <ListItem button onClick={this.filter}>
                  <ListItemText inset primary={value} />
                </ListItem>
              </List>
            );
          })}

        </Collapse>
      </List>
    );
  }
}

export default FilterItem;
