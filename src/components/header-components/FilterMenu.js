import React, {Component} from 'react';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import FilterItem from './FilterItem';
class FilterMenu extends Component {
  state = {
    anchorEl: null,
  };

  handleClick = event => {
    this.setState ({anchorEl: event.currentTarget});
  };

  filterResult=val=>{
    this.setState ({anchorEl: null});
    this.props.filterInputValue (val);
  }

  render () {
    const {anchorEl} = this.state;
    return (
      <div>
        <Button onClick={this.handleClick}>
          Filter
        </Button>
        <Menu
          id="filter-menu"
          anchorEl={anchorEl}
          open={Boolean (anchorEl)}
        >

          <FilterItem title="Author" filterMenuData={this.props.filterMenuData.author} filterInputValue={this.filterResult}/>

          <FilterItem title="State" filterMenuData={this.props.filterMenuData.state} filterInputValue={this.filterResult}/>

          <FilterItem title="Labels"  filterMenuData={this.props.filterMenuData.labels} filterInputValue={this.filterResult}/>

          >
        </Menu>
      </div>
    );
  }
}

export default FilterMenu;
