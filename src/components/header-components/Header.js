import React, {Component} from 'react';

import './header.css';
import SortMenu from './SortMenu';
import FilterMenu from './FilterMenu';
import Search from './SearchComponent'

class Header extends Component {
  sortResult=val=>{
    this.props.sortInputValue (val);
  }
  filterResult=val=>{
    this.props.filterInputValue (val);
  }
  searchResult=val=>{
    this.props.searchInputValue (val);
  }

  render () {
     return (
      <div className="header-section">
        <h3>Issues</h3>
        <Search searchInputValue={this.searchResult}/>
        <FilterMenu filterMenuData={this.props.filterMenuData} filterInputValue={this.filterResult}/>
        <SortMenu sortInputValue={this.sortResult}/>
      </div>

    );
  }
}

export default Header;
