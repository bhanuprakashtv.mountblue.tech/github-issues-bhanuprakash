import React, {Component} from 'react';
import SearchIcon from '@material-ui/icons/Search';
import InputBase from '@material-ui/core/InputBase';

import './header.css'



class Search extends Component {
  state = {};

  handleSearch = e => {
    this.props.searchInputValue(e.target.value);
  };

  render () {
    return (
      <div className="search">
        <SearchIcon />
        <InputBase onChange={this.handleSearch} placeholder="Search…" />
      </div>
    );
  }
}

export default Search;
