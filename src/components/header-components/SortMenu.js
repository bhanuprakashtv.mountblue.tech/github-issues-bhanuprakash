import React, {Component} from 'react';
import Button from '@material-ui/core/Button';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';

class SortMenu extends Component {
  state = {
    anchorEl: null,
  };

  handleClick = event => {
    this.setState ({anchorEl: event.currentTarget});
  };

  handleClose = e => {
    this.setState ({anchorEl: null});
    this.props.sortInputValue (e.target.textContent);
  };
  render () {
    const {anchorEl} = this.state;
    return (
      <div>
        <Button onClick={this.handleClick}>
          Sort
        </Button>
        <Menu
          id="simple-menu"
          anchorEl={anchorEl}
          open={Boolean (anchorEl)}
          onClose={this.handleClose}
        >
          <MenuItem onClick={this.handleClose}>newest</MenuItem>
          <MenuItem onClick={this.handleClose}>oldest</MenuItem>
          <MenuItem onClick={this.handleClose}>recently updated</MenuItem>
          <MenuItem onClick={this.handleClose}>
            least recently updated
          </MenuItem>
        </Menu>
      </div>
    );
  }
}

export default SortMenu;
